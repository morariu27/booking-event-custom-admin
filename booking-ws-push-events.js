var http = require('http');
var fs = require('fs');
var path = require('path');

http.createServer(function (request, response) {
  console.log('request ', request.url);

  var filePath = '.' + request.url;
  if (filePath == './')
    filePath = './index.html';

  var extname = String(path.extname(filePath)).toLowerCase();
  var contentType = 'text/html';
  var mimeTypes = {
    '.html': 'text/html',
    '.js': 'text/javascript',
    '.css': 'text/css',
    '.ico': 'image/x-icon',
    '.json': 'application/json',
    '.png': 'image/png',
    '.jpg': 'image/jpg',
    '.gif': 'image/gif',
    '.wav': 'audio/wav',
    '.mp4': 'video/mp4',
    '.woff': 'application/font-woff',
    '.ttf': 'application/font-ttf',
    '.eot': 'application/vnd.ms-fontobject',
    '.otf': 'application/font-otf',
    '.svg': 'application/image/svg+xml'
  };

  contentType = mimeTypes[extname] || 'application/octet-stream';

  fs.readFile(filePath, function(error, content) {
    if (error) {
      if(error.code == 'ENOENT'){
        fs.readFile('./404.html', function(error, content) {
            response.writeHead(200, { 'Content-Type': contentType });
            response.end(content, 'utf-8');
        });
      }
      else {
        response.writeHead(500);
        response.end('Sorry, check with the site admin for error: '+error.code+' ..\n');
        response.end();
      }
    }
    else {
      response.writeHead(200, { 'Content-Type': contentType });
      response.end(content, 'utf-8');
    }
  });

}).listen(8080, '192.168.1.109');
console.log('Server running at 192.168.1.109:8080/');
// up to here this is how you serve static files without express.js, just with default node server

// create the web socket on a different port
const WebSocket = require('ws');
const wss = new WebSocket.Server({ port: 8081 });

var musicEvent = {
    "name": {
        "text": "50 Cent is coming to sing here!!!", 
        "html": "Curtis James Jackson III (born July 6, 1975), known professionally as 50 Cent, is an American rapper, actor, businessman, and investor"
    }, 
    "description": {
        "text": "\u00a0\r\n\r\n\u00a0\r\nKim Kardashian\r\n(LIVE ON V103)\u00a0\r\n1100 Crescent Ave.\r\nAtlanta, Ga. 30309\r\n\u00a0\r\nLADIES FREE ALL NIGHT / GUYS UNTIL MIDNIGHT\u00a0\r\n\u00a0\r\nRSVP AT www.TruthOnSaturday.eventbrite.com\u00a0\r\n\u00a0\r\nGREAT MUSIC - GREAT FOOD - HOOKAH - \u00a0PREMIUM DRINK SPECIALS\u00a0- & MORE....\r\n\u00a0\u00a0to book your Bottle Service Experience\r\nPLEASE TXT\u00a0770-299-3166\r\n\u00a0DOORS FOR THIS EVENT OPEN\u00a0AT 10PM\r\nMake sure to Follow @CirocBoyManny on INSTAGRAM & SNAPCHAT for updates\u00a0and future events.\r\n", 
        "html": "<P STYLE=\"text-align: center;\">\u00a0<\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"text-decoration: underline; font-size: xx-large;\"><STRONG><SPAN STYLE=\"font-family: Helvetica, sans-serif;\"><BR><\/SPAN><\/STRONG><\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\">\u00a0<\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large;\"><STRONG><SPAN STYLE=\"font-family: Helvetica, sans-serif;\">TRUTH NIGHTCLUB<\/SPAN><\/STRONG><\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large;\"><STRONG><SPAN STYLE=\"font-family: Helvetica, sans-serif;\">(LIVE ON V103)\u00a0<\/SPAN><\/STRONG><\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large;\"><STRONG><SPAN STYLE=\"font-family: Helvetica, sans-serif;\">1100 Crescent Ave.<\/SPAN><\/STRONG><\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large;\"><STRONG><SPAN STYLE=\"font-family: Helvetica, sans-serif;\">Atlanta, Ga. 30309<\/SPAN><\/STRONG><\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large; color: #ff0000;\">\u00a0<\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large; color: #ff0000;\">LADIES FREE ALL NIGHT / GUYS UNTIL MIDNIGHT<\/SPAN>\u00a0<\/P>\r\n<P STYLE=\"text-align: center;\"><STRONG>\u00a0<\/STRONG><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large;\"><STRONG>RSVP AT www.TruthOnSaturday.eventbrite.com<\/STRONG>\u00a0<\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\">\u00a0<\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-family: Helvetica, sans-serif; font-size: x-large;\">GREAT MUSIC - GREAT FOOD - HOOKAH - \u00a0PREMIUM DRINK SPECIALS\u00a0- &amp; MORE....<\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large;\"><SPAN STYLE=\"font-family: Helvetica, sans-serif;\">\u00a0<\/SPAN><SPAN STYLE=\"font-family: Helvetica, sans-serif; line-height: 1.6em;\">\u00a0to book your Bottle Service Experience<\/SPAN><\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-family: Helvetica, sans-serif; font-size: x-large;\">PLEASE TXT<\/SPAN><SPAN STYLE=\"font-family: Helvetica, sans-serif; font-size: x-large;\">\u00a0770-299-3166<\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large;\"><B><SPAN STYLE=\"font-family: Helvetica, sans-serif; color: #666666; letter-spacing: 0.4pt;\">\u00a0DOORS FOR THIS EVENT OPEN\u00a0AT 10PM<\/SPAN><\/B><\/SPAN><\/P>\r\n<P STYLE=\"line-height: 19.2pt; text-align: center;\"><SPAN STYLE=\"font-size: x-large;\"><STRONG><SPAN STYLE=\"font-family: Helvetica, sans-serif; color: #666666; letter-spacing: 0.4pt;\">Make sure to Follow @CirocBoyManny on INSTAGRAM &amp; SNAPCHAT for updates\u00a0and future events.<\/SPAN><\/STRONG><\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><BR><BR><\/P>"
    }, 
    "id": "10609472217", 
    "url": "https://www.eventbrite.com/e/greek-weekend-ladies-greeks-free-all-night-tickets-10609472217?aff=ebapi", 
    "vanity_url": "https://truthonsaturday.eventbrite.com", 
    "start": {
        "timezone": "America/New_York", 
        "local": "2017-06-24T22:00:00", 
        "utc": "2017-06-25T02:00:00Z"
    }, 
    "end": {
        "timezone": "America/New_York", 
        "local": "2017-06-25T03:00:00", 
        "utc": "2017-06-25T07:00:00Z"
    }, 
    "created": "2014-02-12T22:49:12Z", 
    "changed": "2017-06-23T08:18:18Z", 
    "capacity": 731081, 
    "capacity_is_custom": true, 
    "status": "live", 
    "currency": "USD", 
    "listed": true, 
    "shareable": true, 
    "online_event": false, 
    "tx_time_limit": 480, 
    "hide_start_date": true, 
    "hide_end_date": false, 
    "locale": "en_US", 
    "is_locked": false, 
    "privacy_setting": "unlocked", 
    "is_series": false, 
    "is_series_parent": false, 
    "is_reserved_seating": false, 
    "source": "create_2.0", 
    "is_free": false, 
    "version": "3.0.0", 
    "logo_id": "32638718", 
    "organizer_id": "11254450519", 
    "venue_id": "19719554", 
    "category_id": "110", 
    "subcategory_id": "10001", 
    "format_id": "11", 
    "resource_uri": "https://www.eventbriteapi.com/v3/events/10609472217/", 
    "logo": {
        "crop_mask": {
            "top_left": {
                "x": 0, 
                "y": 141
            }, 
            "width": 640, 
            "height": 320
        }, 
        "original": {
            "url": "http://cdn.hiphoplead.com/static/2012/05/50-Cent1.jpg", 
            "width": 640, 
            "height": 640
        }, 
        "id": "32638718", 
        "url": "http://i.onionstatic.com/avclub/5490/44/16x9/960.jpg", 
        "aspect_ratio": "2", 
        "edge_color": "#a29086", 
        "edge_color_set": true
    }
};


var foodDrink = {
    "name": {
        "text": "Wedding buffet", 
        "html": "There are lots of creative wedding buffet menus that are budget-friendly and will make your reception unique. Take a look and get inspired by 14 DIY wedding ideas for buffets that will please you and your guests."
    }, 
    "description": {
        "text": "\u00a0\r\n\r\n\u00a0\r\nKim Kardashian\r\n(LIVE ON V103)\u00a0\r\n1100 Crescent Ave.\r\nAtlanta, Ga. 30309\r\n\u00a0\r\nLADIES FREE ALL NIGHT / GUYS UNTIL MIDNIGHT\u00a0\r\n\u00a0\r\nRSVP AT www.TruthOnSaturday.eventbrite.com\u00a0\r\n\u00a0\r\nGREAT MUSIC - GREAT FOOD - HOOKAH - \u00a0PREMIUM DRINK SPECIALS\u00a0- & MORE....\r\n\u00a0\u00a0to book your Bottle Service Experience\r\nPLEASE TXT\u00a0770-299-3166\r\n\u00a0DOORS FOR THIS EVENT OPEN\u00a0AT 10PM\r\nMake sure to Follow @CirocBoyManny on INSTAGRAM & SNAPCHAT for updates\u00a0and future events.\r\n", 
        "html": "<P STYLE=\"text-align: center;\">\u00a0<\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"text-decoration: underline; font-size: xx-large;\"><STRONG><SPAN STYLE=\"font-family: Helvetica, sans-serif;\"><BR><\/SPAN><\/STRONG><\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\">\u00a0<\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large;\"><STRONG><SPAN STYLE=\"font-family: Helvetica, sans-serif;\">TRUTH NIGHTCLUB<\/SPAN><\/STRONG><\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large;\"><STRONG><SPAN STYLE=\"font-family: Helvetica, sans-serif;\">(LIVE ON V103)\u00a0<\/SPAN><\/STRONG><\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large;\"><STRONG><SPAN STYLE=\"font-family: Helvetica, sans-serif;\">1100 Crescent Ave.<\/SPAN><\/STRONG><\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large;\"><STRONG><SPAN STYLE=\"font-family: Helvetica, sans-serif;\">Atlanta, Ga. 30309<\/SPAN><\/STRONG><\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large; color: #ff0000;\">\u00a0<\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large; color: #ff0000;\">LADIES FREE ALL NIGHT / GUYS UNTIL MIDNIGHT<\/SPAN>\u00a0<\/P>\r\n<P STYLE=\"text-align: center;\"><STRONG>\u00a0<\/STRONG><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large;\"><STRONG>RSVP AT www.TruthOnSaturday.eventbrite.com<\/STRONG>\u00a0<\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\">\u00a0<\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-family: Helvetica, sans-serif; font-size: x-large;\">GREAT MUSIC - GREAT FOOD - HOOKAH - \u00a0PREMIUM DRINK SPECIALS\u00a0- &amp; MORE....<\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large;\"><SPAN STYLE=\"font-family: Helvetica, sans-serif;\">\u00a0<\/SPAN><SPAN STYLE=\"font-family: Helvetica, sans-serif; line-height: 1.6em;\">\u00a0to book your Bottle Service Experience<\/SPAN><\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-family: Helvetica, sans-serif; font-size: x-large;\">PLEASE TXT<\/SPAN><SPAN STYLE=\"font-family: Helvetica, sans-serif; font-size: x-large;\">\u00a0770-299-3166<\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large;\"><B><SPAN STYLE=\"font-family: Helvetica, sans-serif; color: #666666; letter-spacing: 0.4pt;\">\u00a0DOORS FOR THIS EVENT OPEN\u00a0AT 10PM<\/SPAN><\/B><\/SPAN><\/P>\r\n<P STYLE=\"line-height: 19.2pt; text-align: center;\"><SPAN STYLE=\"font-size: x-large;\"><STRONG><SPAN STYLE=\"font-family: Helvetica, sans-serif; color: #666666; letter-spacing: 0.4pt;\">Make sure to Follow @CirocBoyManny on INSTAGRAM &amp; SNAPCHAT for updates\u00a0and future events.<\/SPAN><\/STRONG><\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><BR><BR><\/P>"
    }, 
    "id": "10609472217", 
    "url": "https://www.eventbrite.com/e/greek-weekend-ladies-greeks-free-all-night-tickets-10609472217?aff=ebapi", 
    "vanity_url": "https://truthonsaturday.eventbrite.com", 
    "start": {
        "timezone": "America/New_York", 
        "local": "2017-06-24T22:00:00", 
        "utc": "2017-06-25T02:00:00Z"
    }, 
    "end": {
        "timezone": "America/New_York", 
        "local": "2017-06-25T03:00:00", 
        "utc": "2017-06-25T07:00:00Z"
    }, 
    "created": "2014-02-12T22:49:12Z", 
    "changed": "2017-06-23T08:18:18Z", 
    "capacity": 731081, 
    "capacity_is_custom": true, 
    "status": "live", 
    "currency": "USD", 
    "listed": true, 
    "shareable": true, 
    "online_event": false, 
    "tx_time_limit": 480, 
    "hide_start_date": true, 
    "hide_end_date": false, 
    "locale": "en_US", 
    "is_locked": false, 
    "privacy_setting": "unlocked", 
    "is_series": false, 
    "is_series_parent": false, 
    "is_reserved_seating": false, 
    "source": "create_2.0", 
    "is_free": false, 
    "version": "3.0.0", 
    "logo_id": "32638718", 
    "organizer_id": "11254450519", 
    "venue_id": "19719554", 
    "category_id": "110", 
    "subcategory_id": "10001", 
    "format_id": "11", 
    "resource_uri": "https://www.eventbriteapi.com/v3/events/10609472217/", 
    "logo": {
        "crop_mask": {
            "top_left": {
                "x": 0, 
                "y": 141
            }, 
            "width": 640, 
            "height": 320
        }, 
        "original": {
            "url": "https://www.cheerschalet.com/wp-content/uploads/2012/01/wedding-buffet.jpg", 
            "width": 640, 
            "height": 640
        }, 
        "id": "32638718", 
        "url": "https://www.cheerschalet.com/wp-content/uploads/2012/01/wedding-buffet.jpg", 
        "aspect_ratio": "2", 
        "edge_color": "#a29086", 
        "edge_color_set": true
    }
};

var businessProfesionall = {
    "name": {
        "text": "Business Professionall Event.", 
        "html": "Five bustling boroughs make up the powerhouse known as New York City. Iconic sites like the Empire State Building and the Statue of Liberty draw crowds from across the globe."
    }, 
    "description": {
        "text": "\u00a0\r\n\r\n\u00a0\r\nKim Kardashian\r\n(LIVE ON V103)\u00a0\r\n1100 Crescent Ave.\r\nAtlanta, Ga. 30309\r\n\u00a0\r\nLADIES FREE ALL NIGHT / GUYS UNTIL MIDNIGHT\u00a0\r\n\u00a0\r\nRSVP AT www.TruthOnSaturday.eventbrite.com\u00a0\r\n\u00a0\r\nGREAT MUSIC - GREAT FOOD - HOOKAH - \u00a0PREMIUM DRINK SPECIALS\u00a0- & MORE....\r\n\u00a0\u00a0to book your Bottle Service Experience\r\nPLEASE TXT\u00a0770-299-3166\r\n\u00a0DOORS FOR THIS EVENT OPEN\u00a0AT 10PM\r\nMake sure to Follow @CirocBoyManny on INSTAGRAM & SNAPCHAT for updates\u00a0and future events.\r\n", 
        "html": "<P STYLE=\"text-align: center;\">\u00a0<\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"text-decoration: underline; font-size: xx-large;\"><STRONG><SPAN STYLE=\"font-family: Helvetica, sans-serif;\"><BR><\/SPAN><\/STRONG><\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\">\u00a0<\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large;\"><STRONG><SPAN STYLE=\"font-family: Helvetica, sans-serif;\">TRUTH NIGHTCLUB<\/SPAN><\/STRONG><\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large;\"><STRONG><SPAN STYLE=\"font-family: Helvetica, sans-serif;\">(LIVE ON V103)\u00a0<\/SPAN><\/STRONG><\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large;\"><STRONG><SPAN STYLE=\"font-family: Helvetica, sans-serif;\">1100 Crescent Ave.<\/SPAN><\/STRONG><\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large;\"><STRONG><SPAN STYLE=\"font-family: Helvetica, sans-serif;\">Atlanta, Ga. 30309<\/SPAN><\/STRONG><\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large; color: #ff0000;\">\u00a0<\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large; color: #ff0000;\">LADIES FREE ALL NIGHT / GUYS UNTIL MIDNIGHT<\/SPAN>\u00a0<\/P>\r\n<P STYLE=\"text-align: center;\"><STRONG>\u00a0<\/STRONG><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large;\"><STRONG>RSVP AT www.TruthOnSaturday.eventbrite.com<\/STRONG>\u00a0<\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\">\u00a0<\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-family: Helvetica, sans-serif; font-size: x-large;\">GREAT MUSIC - GREAT FOOD - HOOKAH - \u00a0PREMIUM DRINK SPECIALS\u00a0- &amp; MORE....<\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large;\"><SPAN STYLE=\"font-family: Helvetica, sans-serif;\">\u00a0<\/SPAN><SPAN STYLE=\"font-family: Helvetica, sans-serif; line-height: 1.6em;\">\u00a0to book your Bottle Service Experience<\/SPAN><\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-family: Helvetica, sans-serif; font-size: x-large;\">PLEASE TXT<\/SPAN><SPAN STYLE=\"font-family: Helvetica, sans-serif; font-size: x-large;\">\u00a0770-299-3166<\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large;\"><B><SPAN STYLE=\"font-family: Helvetica, sans-serif; color: #666666; letter-spacing: 0.4pt;\">\u00a0DOORS FOR THIS EVENT OPEN\u00a0AT 10PM<\/SPAN><\/B><\/SPAN><\/P>\r\n<P STYLE=\"line-height: 19.2pt; text-align: center;\"><SPAN STYLE=\"font-size: x-large;\"><STRONG><SPAN STYLE=\"font-family: Helvetica, sans-serif; color: #666666; letter-spacing: 0.4pt;\">Make sure to Follow @CirocBoyManny on INSTAGRAM &amp; SNAPCHAT for updates\u00a0and future events.<\/SPAN><\/STRONG><\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><BR><BR><\/P>"
    }, 
    "id": "10609472217", 
    "url": "https://www.eventbrite.com/e/greek-weekend-ladies-greeks-free-all-night-tickets-10609472217?aff=ebapi", 
    "vanity_url": "https://truthonsaturday.eventbrite.com", 
    "start": {
        "timezone": "America/New_York", 
        "local": "2017-06-24T22:00:00", 
        "utc": "2017-06-25T02:00:00Z"
    }, 
    "end": {
        "timezone": "America/New_York", 
        "local": "2017-06-25T03:00:00", 
        "utc": "2017-06-25T07:00:00Z"
    }, 
    "created": "2014-02-12T22:49:12Z", 
    "changed": "2017-06-23T08:18:18Z", 
    "capacity": 731081, 
    "capacity_is_custom": true, 
    "status": "live", 
    "currency": "USD", 
    "listed": true, 
    "shareable": true, 
    "online_event": false, 
    "tx_time_limit": 480, 
    "hide_start_date": true, 
    "hide_end_date": false, 
    "locale": "en_US", 
    "is_locked": false, 
    "privacy_setting": "unlocked", 
    "is_series": false, 
    "is_series_parent": false, 
    "is_reserved_seating": false, 
    "source": "create_2.0", 
    "is_free": false, 
    "version": "3.0.0", 
    "logo_id": "32638718", 
    "organizer_id": "11254450519", 
    "venue_id": "19719554", 
    "category_id": "110", 
    "subcategory_id": "10001", 
    "format_id": "11", 
    "resource_uri": "https://www.eventbriteapi.com/v3/events/10609472217/", 
    "logo": {
        "crop_mask": {
            "top_left": {
                "x": 0, 
                "y": 141
            }, 
            "width": 640, 
            "height": 320
        }, 
        "original": {
            "url": "http://nyc.gooffsite.com/wp-content/uploads/2013/01/382535_399310290125401_1676261342_n.jpg", 
            "width": 640, 
            "height": 640
        }, 
        "id": "32638718", 
        "url": "http://nyc.gooffsite.com/wp-content/uploads/2013/01/382535_399310290125401_1676261342_n.jpg", 
        "aspect_ratio": "2", 
        "edge_color": "#a29086", 
        "edge_color_set": true
    }
};

var communityCulture = {
    "name": {
        "text": "Community and culture", 
        "html": "During the month of April, Wellesley celebrates Latin@ Month with performances, lectures, panels, workshops, discussions, films and more.  Together, the College honors the cultural diversity that Latin@ students, faculty and staff bring to the Wellesley community as well as raise awareness about issues affecting the Latina community here at Wellesley and beyond. "
    }, 
    "description": {
        "text": "\u00a0\r\n\r\n\u00a0\r\nKim Kardashian\r\n(LIVE ON V103)\u00a0\r\n1100 Crescent Ave.\r\nAtlanta, Ga. 30309\r\n\u00a0\r\nLADIES FREE ALL NIGHT / GUYS UNTIL MIDNIGHT\u00a0\r\n\u00a0\r\nRSVP AT www.TruthOnSaturday.eventbrite.com\u00a0\r\n\u00a0\r\nGREAT MUSIC - GREAT FOOD - HOOKAH - \u00a0PREMIUM DRINK SPECIALS\u00a0- & MORE....\r\n\u00a0\u00a0to book your Bottle Service Experience\r\nPLEASE TXT\u00a0770-299-3166\r\n\u00a0DOORS FOR THIS EVENT OPEN\u00a0AT 10PM\r\nMake sure to Follow @CirocBoyManny on INSTAGRAM & SNAPCHAT for updates\u00a0and future events.\r\n", 
        "html": "<P STYLE=\"text-align: center;\">\u00a0<\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"text-decoration: underline; font-size: xx-large;\"><STRONG><SPAN STYLE=\"font-family: Helvetica, sans-serif;\"><BR><\/SPAN><\/STRONG><\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\">\u00a0<\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large;\"><STRONG><SPAN STYLE=\"font-family: Helvetica, sans-serif;\">TRUTH NIGHTCLUB<\/SPAN><\/STRONG><\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large;\"><STRONG><SPAN STYLE=\"font-family: Helvetica, sans-serif;\">(LIVE ON V103)\u00a0<\/SPAN><\/STRONG><\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large;\"><STRONG><SPAN STYLE=\"font-family: Helvetica, sans-serif;\">1100 Crescent Ave.<\/SPAN><\/STRONG><\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large;\"><STRONG><SPAN STYLE=\"font-family: Helvetica, sans-serif;\">Atlanta, Ga. 30309<\/SPAN><\/STRONG><\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large; color: #ff0000;\">\u00a0<\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large; color: #ff0000;\">LADIES FREE ALL NIGHT / GUYS UNTIL MIDNIGHT<\/SPAN>\u00a0<\/P>\r\n<P STYLE=\"text-align: center;\"><STRONG>\u00a0<\/STRONG><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large;\"><STRONG>RSVP AT www.TruthOnSaturday.eventbrite.com<\/STRONG>\u00a0<\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\">\u00a0<\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-family: Helvetica, sans-serif; font-size: x-large;\">GREAT MUSIC - GREAT FOOD - HOOKAH - \u00a0PREMIUM DRINK SPECIALS\u00a0- &amp; MORE....<\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large;\"><SPAN STYLE=\"font-family: Helvetica, sans-serif;\">\u00a0<\/SPAN><SPAN STYLE=\"font-family: Helvetica, sans-serif; line-height: 1.6em;\">\u00a0to book your Bottle Service Experience<\/SPAN><\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-family: Helvetica, sans-serif; font-size: x-large;\">PLEASE TXT<\/SPAN><SPAN STYLE=\"font-family: Helvetica, sans-serif; font-size: x-large;\">\u00a0770-299-3166<\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large;\"><B><SPAN STYLE=\"font-family: Helvetica, sans-serif; color: #666666; letter-spacing: 0.4pt;\">\u00a0DOORS FOR THIS EVENT OPEN\u00a0AT 10PM<\/SPAN><\/B><\/SPAN><\/P>\r\n<P STYLE=\"line-height: 19.2pt; text-align: center;\"><SPAN STYLE=\"font-size: x-large;\"><STRONG><SPAN STYLE=\"font-family: Helvetica, sans-serif; color: #666666; letter-spacing: 0.4pt;\">Make sure to Follow @CirocBoyManny on INSTAGRAM &amp; SNAPCHAT for updates\u00a0and future events.<\/SPAN><\/STRONG><\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><BR><BR><\/P>"
    }, 
    "id": "10609472217", 
    "url": "https://www.eventbrite.com/e/greek-weekend-ladies-greeks-free-all-night-tickets-10609472217?aff=ebapi", 
    "vanity_url": "https://truthonsaturday.eventbrite.com", 
    "start": {
        "timezone": "America/New_York", 
        "local": "2017-06-24T22:00:00", 
        "utc": "2017-06-25T02:00:00Z"
    }, 
    "end": {
        "timezone": "America/New_York", 
        "local": "2017-06-25T03:00:00", 
        "utc": "2017-06-25T07:00:00Z"
    }, 
    "created": "2014-02-12T22:49:12Z", 
    "changed": "2017-06-23T08:18:18Z", 
    "capacity": 731081, 
    "capacity_is_custom": true, 
    "status": "live", 
    "currency": "USD", 
    "listed": true, 
    "shareable": true, 
    "online_event": false, 
    "tx_time_limit": 480, 
    "hide_start_date": true, 
    "hide_end_date": false, 
    "locale": "en_US", 
    "is_locked": false, 
    "privacy_setting": "unlocked", 
    "is_series": false, 
    "is_series_parent": false, 
    "is_reserved_seating": false, 
    "source": "create_2.0", 
    "is_free": false, 
    "version": "3.0.0", 
    "logo_id": "32638718", 
    "organizer_id": "11254450519", 
    "venue_id": "19719554", 
    "category_id": "110", 
    "subcategory_id": "10001", 
    "format_id": "11", 
    "resource_uri": "https://www.eventbriteapi.com/v3/events/10609472217/", 
    "logo": {
        "crop_mask": {
            "top_left": {
                "x": 0, 
                "y": 141
            }, 
            "width": 640, 
            "height": 320
        }, 
        "original": {
            "url": "http://cdn.images.express.co.uk/img/dynamic/galleries/x701/161048.jpg", 
            "width": 640, 
            "height": 640
        }, 
        "id": "32638718", 
        "url": "https://www.wellesley.edu/sites/default/files/assets/dailyshot/2015/ds_930x475_1.jpg", 
        "aspect_ratio": "2", 
        "edge_color": "#a29086", 
        "edge_color_set": true
    }
};

var performingVisualArts = {
    "name": {
        "text": "Visual Arts", 
        "html": "Kim Karadashian will have a concert at the location!!! Come see her live!!!"
    }, 
    "description": {
        "text": "\u00a0\r\n\r\n\u00a0\r\nKim Kardashian\r\n(LIVE ON V103)\u00a0\r\n1100 Crescent Ave.\r\nAtlanta, Ga. 30309\r\n\u00a0\r\nLADIES FREE ALL NIGHT / GUYS UNTIL MIDNIGHT\u00a0\r\n\u00a0\r\nRSVP AT www.TruthOnSaturday.eventbrite.com\u00a0\r\n\u00a0\r\nGREAT MUSIC - GREAT FOOD - HOOKAH - \u00a0PREMIUM DRINK SPECIALS\u00a0- & MORE....\r\n\u00a0\u00a0to book your Bottle Service Experience\r\nPLEASE TXT\u00a0770-299-3166\r\n\u00a0DOORS FOR THIS EVENT OPEN\u00a0AT 10PM\r\nMake sure to Follow @CirocBoyManny on INSTAGRAM & SNAPCHAT for updates\u00a0and future events.\r\n", 
        "html": "<P STYLE=\"text-align: center;\">\u00a0<\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"text-decoration: underline; font-size: xx-large;\"><STRONG><SPAN STYLE=\"font-family: Helvetica, sans-serif;\"><BR><\/SPAN><\/STRONG><\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\">\u00a0<\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large;\"><STRONG><SPAN STYLE=\"font-family: Helvetica, sans-serif;\">TRUTH NIGHTCLUB<\/SPAN><\/STRONG><\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large;\"><STRONG><SPAN STYLE=\"font-family: Helvetica, sans-serif;\">(LIVE ON V103)\u00a0<\/SPAN><\/STRONG><\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large;\"><STRONG><SPAN STYLE=\"font-family: Helvetica, sans-serif;\">1100 Crescent Ave.<\/SPAN><\/STRONG><\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large;\"><STRONG><SPAN STYLE=\"font-family: Helvetica, sans-serif;\">Atlanta, Ga. 30309<\/SPAN><\/STRONG><\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large; color: #ff0000;\">\u00a0<\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large; color: #ff0000;\">LADIES FREE ALL NIGHT / GUYS UNTIL MIDNIGHT<\/SPAN>\u00a0<\/P>\r\n<P STYLE=\"text-align: center;\"><STRONG>\u00a0<\/STRONG><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large;\"><STRONG>RSVP AT www.TruthOnSaturday.eventbrite.com<\/STRONG>\u00a0<\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\">\u00a0<\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-family: Helvetica, sans-serif; font-size: x-large;\">GREAT MUSIC - GREAT FOOD - HOOKAH - \u00a0PREMIUM DRINK SPECIALS\u00a0- &amp; MORE....<\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large;\"><SPAN STYLE=\"font-family: Helvetica, sans-serif;\">\u00a0<\/SPAN><SPAN STYLE=\"font-family: Helvetica, sans-serif; line-height: 1.6em;\">\u00a0to book your Bottle Service Experience<\/SPAN><\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-family: Helvetica, sans-serif; font-size: x-large;\">PLEASE TXT<\/SPAN><SPAN STYLE=\"font-family: Helvetica, sans-serif; font-size: x-large;\">\u00a0770-299-3166<\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><SPAN STYLE=\"font-size: x-large;\"><B><SPAN STYLE=\"font-family: Helvetica, sans-serif; color: #666666; letter-spacing: 0.4pt;\">\u00a0DOORS FOR THIS EVENT OPEN\u00a0AT 10PM<\/SPAN><\/B><\/SPAN><\/P>\r\n<P STYLE=\"line-height: 19.2pt; text-align: center;\"><SPAN STYLE=\"font-size: x-large;\"><STRONG><SPAN STYLE=\"font-family: Helvetica, sans-serif; color: #666666; letter-spacing: 0.4pt;\">Make sure to Follow @CirocBoyManny on INSTAGRAM &amp; SNAPCHAT for updates\u00a0and future events.<\/SPAN><\/STRONG><\/SPAN><\/P>\r\n<P STYLE=\"text-align: center;\"><BR><BR><\/P>"
    }, 
    "id": "10609472217", 
    "url": "https://www.eventbrite.com/e/greek-weekend-ladies-greeks-free-all-night-tickets-10609472217?aff=ebapi", 
    "vanity_url": "https://truthonsaturday.eventbrite.com", 
    "start": {
        "timezone": "America/New_York", 
        "local": "2017-06-24T22:00:00", 
        "utc": "2017-06-25T02:00:00Z"
    }, 
    "end": {
        "timezone": "America/New_York", 
        "local": "2017-06-25T03:00:00", 
        "utc": "2017-06-25T07:00:00Z"
    }, 
    "created": "2014-02-12T22:49:12Z", 
    "changed": "2017-06-23T08:18:18Z", 
    "capacity": 731081, 
    "capacity_is_custom": true, 
    "status": "live", 
    "currency": "USD", 
    "listed": true, 
    "shareable": true, 
    "online_event": false, 
    "tx_time_limit": 480, 
    "hide_start_date": true, 
    "hide_end_date": false, 
    "locale": "en_US", 
    "is_locked": false, 
    "privacy_setting": "unlocked", 
    "is_series": false, 
    "is_series_parent": false, 
    "is_reserved_seating": false, 
    "source": "create_2.0", 
    "is_free": false, 
    "version": "3.0.0", 
    "logo_id": "32638718", 
    "organizer_id": "11254450519", 
    "venue_id": "19719554", 
    "category_id": "110", 
    "subcategory_id": "10001", 
    "format_id": "11", 
    "resource_uri": "https://www.eventbriteapi.com/v3/events/10609472217/", 
    "logo": {
        "crop_mask": {
            "top_left": {
                "x": 0, 
                "y": 141
            }, 
            "width": 640, 
            "height": 320
        }, 
        "original": {
            "url": "http://cdn.images.express.co.uk/img/dynamic/galleries/x701/161048.jpg", 
            "width": 640, 
            "height": 640
        }, 
        "id": "32638718", 
        "url": "https://ae01.alicdn.com/wsphoto/v0/686412640/Free-shipping-Kim-Cardashian-Celebrity-dress-Party-dress-Bandage-Dress-Long-Sleeve-Sheath-Knee-Length-Custome.jpg", 
        "aspect_ratio": "2", 
        "edge_color": "#a29086", 
        "edge_color_set": true
    }
};

wss.on('connection', function connection(ws) {
  ws.on('message', function incoming(data) {
    console.log(JSON.parse(data).sparta,'wtf');
    if(JSON.parse(data).sparta == "food-event" ) {
      ws.send(JSON.stringify(foodDrink));
    } else if(JSON.parse(data).sparta == "business-event" ) {
      ws.send(JSON.stringify(businessProfesionall));
    } else if(JSON.parse(data).sparta == "music-event" ) {
      ws.send(JSON.stringify(musicEvent));
    } else if(JSON.parse(data).sparta == "community-culture" ) {
      ws.send(JSON.stringify(communityCulture));
    } else if(JSON.parse(data).sparta == "visual-arts" ) {
      ws.send(JSON.stringify(performingVisualArts));
    } else {
      console.log('nothing to send');
    }
  });
});



